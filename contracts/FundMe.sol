// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "@chainlink/contracts/src/v0.8/interfaces/AggregatorV3Interface.sol";
import "@openzeppelin/contracts/utils/Strings.sol";

/**
 * @title Contract to fund my solidiy tutorials project.
 * @notice All the fund goes to the owner!
 */
contract FundMe {
    uint256 constant MIN_AMOUNT_USD = 10;
    uint256 constant MIN_AMOUNT = MIN_AMOUNT_USD * (10**18);
    address OWNER;
    mapping(address => uint256) public fundersToAmount;
    address[] funders;
    AggregatorV3Interface internal priceFeed;

    constructor() {
        priceFeed = AggregatorV3Interface(
            0x9326BFA02ADD2366b30bacB125260Af641031331
        );
        OWNER = msg.sender;
    }

    modifier onlyOwner() {
        require(msg.sender == OWNER, "You are NOT the owner!");
        _;
    }

    function fund() public payable {
        require(
            getConversionRate(msg.value) >= MIN_AMOUNT_USD,
            string(
                abi.encodePacked(
                    "The minimum amount to fund is ",
                    MIN_AMOUNT_USD,
                    " USD"
                )
            )
        );

        fundersToAmount[msg.sender] += msg.value;
        funders.push(msg.sender);
    }

    function withdraw() public payable onlyOwner {
        (bool sent, ) = payable(OWNER).call{value: address(this).balance}("");
        require(sent, "Failed to send Ether");

        // Resetting funders tracking variables
        for (uint256 index = 0; index < funders.length; index++) {
            fundersToAmount[funders[index]] = 0;
        }
        delete funders;
    }

    function getBalance() public view returns (uint256) {
        return address(this).balance;
    }

    /**
     * @return weiPriceInUsd wei price in USD, where 1 ETH = 10**18 wei
     */
    function weiPrice() public view returns (uint256) {
        (, int256 weiPriceInUsd, , , ) = priceFeed.latestRoundData();

        // converting it to wei
        return uint256(weiPriceInUsd * (10**10));
    }

    /**
     * @param weiAmount expected amount of ETH to be converted
     * @return weiAmountInUsd Gewi price in USD
     */
    function getConversionRate(uint256 weiAmount)
        public
        view
        returns (uint256 weiAmountInUsd)
    {
        weiAmountInUsd = (weiAmount * weiPrice()) / (10**18);
    }
}
